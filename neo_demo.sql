-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 03:43 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `neo_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ans`
--

CREATE TABLE `tbl_ans` (
  `id` bigint(255) NOT NULL,
  `user_empid` varchar(255) DEFAULT NULL,
  `qn1` varchar(255) DEFAULT NULL,
  `text1` varchar(255) DEFAULT NULL,
  `qn2` varchar(255) DEFAULT NULL,
  `text2` varchar(255) DEFAULT NULL,
  `qn3` varchar(255) DEFAULT NULL,
  `text3` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ans`
--

INSERT INTO `tbl_ans` (`id`, `user_empid`, `qn1`, `text1`, `qn2`, `text2`, `qn3`, `text3`) VALUES
(1, 'umesh.sathe@siemens.com', '1', 'jiiiii', '2', 'dddd', '3', 'ddd'),
(2, 'umesh.sathe@siemens.com', '1', 'hh', '2', 'ff', '3', 'ff'),
(3, 'umesh.sathe@siemens.com', '1', '', '2', '', '3', ''),
(4, 'umesh.sathe@siemens.com', '1', '', '2', '', '3', ''),
(5, 'LEENA.JAIN@SIEMENS.COM', '1', 'fyn', '2', '', '3', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_answer`
--

CREATE TABLE `tbl_answer` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_answer`
--

INSERT INTO `tbl_answer` (`id`, `question_id`, `answer`) VALUES
(1, 1, 'Facebook'),
(2, 1, 'Twitter'),
(3, 1, 'LinkedIn'),
(4, 1, 'Instagram'),
(5, 2, 'Skiing'),
(6, 2, 'Biking'),
(7, 2, 'Snowboarding'),
(8, 2, 'Surfing'),
(11, 3, 'Bose'),
(12, 3, 'JBL'),
(13, 3, 'Sony'),
(14, 3, 'Samsung');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_poll`
--

CREATE TABLE `tbl_poll` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `eventname` varchar(50) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pollanswers`
--

INSERT INTO `tbl_pollanswers` (`id`, `poll_id`, `users_id`, `poll_answer`, `eventname`, `poll_at`, `points`) VALUES
(1, 2, 25, 'opt1', 'pollsdemo01', '2021-06-22 18:57:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0',
  `eventname` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_polls`
--

INSERT INTO `tbl_polls` (`id`, `poll_question`, `poll_opt1`, `poll_opt2`, `poll_opt3`, `poll_opt4`, `correct_ans`, `active`, `poll_over`, `show_results`, `eventname`) VALUES
(3, 'ss', 'ss', 'ss', 'ss', 'ss', 'opt2', 0, 0, 0, 'pollsdemo01'),
(2, 'xxx', 'xx', 'xx', 'xx', 'xx', 'opt1', 0, 0, 0, 'pollsdemo01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_emailid` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_emailid`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Ms rakshith shetty', 'rakshith@neosoulsoft.com', 'hii', '2021-01-28 15:48:52', 'neodemo', 0, 0),
(2, 'Ms rakshith shetty', '', 'hii', '2021-01-28 15:52:43', 'neodemo', 0, 0),
(3, 'Ms rakshith shetty', 'rakshith@neosoulsoft.com', 'hii', '2021-01-28 15:53:51', 'neodemo', 0, 0),
(4, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'xxx', '2021-07-02 15:42:39', 'pollsdemo01', 1, 1),
(5, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'ss', '2021-07-02 16:58:39', 'pollsdemo01', 0, 0),
(6, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'ss', '2021-07-02 16:58:51', 'pollsdemo01', 0, 0),
(7, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'ss', '2021-07-02 16:59:16', 'pollsdemo01', 1, 1),
(8, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'gg', '2021-07-02 17:17:25', 'pollsdemo01', 0, 0),
(9, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'ss', '2021-07-02 17:46:35', 'pollsdemo01', 0, 0),
(10, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'ss', '2021-07-02 17:50:53', 'pollsdemo01', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reactions`
--

CREATE TABLE `tbl_reactions` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `reaction` varchar(50) NOT NULL,
  `reaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team_chat`
--

CREATE TABLE `tbl_team_chat` (
  `id` int(11) NOT NULL,
  `user_id_from` varchar(20) NOT NULL,
  `user_id_to` varchar(10) NOT NULL,
  `message` varchar(500) NOT NULL,
  `chat_time` datetime NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_team_chat`
--

INSERT INTO `tbl_team_chat` (`id`, `user_id_from`, `user_id_to`, `message`, `chat_time`, `read_status`) VALUES
(1, '25', 'team', 'sss', '2021-06-28 09:55:39', 1),
(2, 'team', '25', 'ss', '2021-06-28 09:56:03', 1),
(3, '25', 'team', 'sss', '2021-06-28 10:13:47', 1),
(4, '25', 'team', 'xxx', '2021-06-28 10:20:18', 1),
(5, '25', 'team', 'ss', '2021-06-28 10:20:40', 1),
(6, '25', 'team', 'sss', '2021-06-28 10:25:22', 1),
(7, '25', 'team', 'ssss', '2021-06-28 10:31:04', 1),
(8, '25', 'team', 'ssss', '2021-06-28 10:31:09', 1),
(9, '25', 'team', 'ssss', '2021-06-28 10:31:10', 1),
(10, '25', 'team', 'ssss', '2021-06-28 10:31:10', 1),
(11, '25', 'team', 'ssss', '2021-06-28 10:31:11', 1),
(12, '25', 'team', 'ssss', '2021-06-28 10:31:12', 1),
(13, '25', 'team', 'ssss', '2021-06-28 10:37:45', 1),
(14, '25', 'team', 'ssss', '2021-06-28 10:37:45', 1),
(15, '25', 'team', 'ssss', '2021-06-28 10:37:45', 1),
(16, '25', 'team', 'ssss', '2021-06-28 10:37:45', 1),
(17, '25', 'team', 'ssss', '2021-06-28 10:37:51', 1),
(18, '25', 'team', 'ssss', '2021-06-28 10:37:52', 1),
(19, '25', 'team', 'ssss', '2021-06-28 10:37:52', 1),
(20, '25', 'team', 'ssss', '2021-06-28 10:37:52', 1),
(21, '25', 'team', 'gggg', '2021-06-28 10:38:26', 1),
(22, '25', 'team', 'vvvvv', '2021-06-28 10:38:48', 1),
(23, '25', 'team', 'ddd', '2021-06-28 10:52:57', 1),
(24, '25', 'team', 'd', '2021-06-28 10:57:40', 1),
(25, '25', 'team', 'sss', '2021-06-28 10:57:43', 1),
(26, '25', 'team', 'hhi', '2021-07-02 17:26:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_location` varchar(500) NOT NULL,
  `user_emailid` varchar(500) NOT NULL,
  `user_mobile` varchar(15) NOT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_location`, `user_emailid`, `user_mobile`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(6, 'PAWAN SHILWANT', 'Mumbai', 'pawan@coact.co.in', '9545329222', '2020-07-21 13:54:01', '2021-01-22 10:17:49', '2021-01-22 10:30:50', 1, 'pollsdemo01'),
(7, 'Pooja', 'Mumbai', 'pooja@coact.co.in', '9768161921', '2020-07-21 14:45:11', '2021-01-15 16:13:14', '2021-01-15 16:47:24', 1, 'pollsdemo01'),
(8, 'Akshat Jharia', 'bangalore', 'akshatjharia@gmail.com', '7204420017', '2020-07-21 15:16:53', '2021-01-13 15:57:41', '2021-01-13 16:04:56', 1, 'pollsdemo01'),
(9, 'DINESH PATADIA', 'MUMBAI', 'dineshp@inceptionx.in', '9892495724', '2020-07-21 15:40:36', '2020-07-21 17:01:13', '2020-07-21 17:15:36', 1, 'pollsdemo01'),
(10, 'Bhupesh ', 'Mumbai ', 'bhupesh@inceptionx.in', '9930708592', '2020-07-21 16:11:46', '2020-07-21 16:58:58', '2020-07-21 17:12:21', 1, 'pollsdemo01'),
(11, 'VIRAJ', 'Pune', 'viraj@coact.co.in', '9765875731', '2020-07-21 17:04:09', '2020-09-24 14:31:33', '2020-09-24 18:11:47', 1, 'pollsdemo01'),
(12, 'Amit', 'Mumbai', 'amit@hexagonevents.com', '9029392185', '2020-09-14 13:20:03', '2020-09-22 15:32:41', '2020-09-22 15:52:24', 0, 'pollsdemo01'),
(13, 'Amit', 'Mumbai', 'amit.parkar91@gmail.com', '9029392185', '2020-09-14 13:21:10', '2020-09-22 15:38:46', '2020-09-22 16:05:05', 1, 'pollsdemo01'),
(14, 'Rahul Soni', 'Mumbai', 'rahulsoni@hexagonevents.com', '9820133946', '2020-09-14 16:02:09', '2020-09-22 15:20:34', '2020-09-22 15:59:40', 0, 'pollsdemo01'),
(15, 'Sujatha ', 'Bangalore ', 'sujatha@coact.co.in', '9845563760', '2020-09-14 17:38:42', '2021-01-15 16:13:39', '2021-01-15 16:37:45', 1, 'pollsdemo01'),
(16, 'ganesh', 'Mumbai', 'ganesh@hexagonevents.com', '9821081887', '2020-09-22 15:01:29', '2020-09-22 15:03:53', '2020-09-22 15:53:39', 0, 'pollsdemo01'),
(17, 'Pankaj rathod', 'Mumbai', 'pankaj@hexagonevents.com', '9930965404', '2020-09-22 15:37:43', '2020-09-22 15:41:20', '2020-09-22 15:55:00', 0, 'pollsdemo01'),
(18, 'lokesh r', 'bangalore', 'lokesh.r@mci-group.com', '09535481989', '2020-10-19 15:08:41', '2020-10-19 15:09:12', '2020-10-19 20:43:21', 1, 'pollsdemo01'),
(19, 'MANU', 'DELHI', 'M@M.COM', '9999999999', '2020-10-19 15:27:04', '2020-10-19 15:27:04', '2020-10-19 15:27:34', 1, 'pollsdemo01'),
(20, 'Manu', 'delhi', 'manu.verma@mci-group.com', '9899998999', '2020-10-19 15:28:41', '2020-10-19 15:29:24', '2020-10-19 17:06:26', 1, 'pollsdemo01'),
(21, 'Nishanth', 'bangalore', 'nishanth@coact.co.in', '8747973536', '2021-01-02 18:59:19', '2021-10-19 15:11:33', '2021-10-19 15:15:08', 0, 'pollsdemo01'),
(22, 'Pooja Jaiswal', 'Mumbai', 'test1@coact.co.in', '09768161921', '2021-01-13 15:58:57', '2021-01-13 15:58:57', '2021-01-13 15:59:27', 1, 'pollsdemo01'),
(23, 'rakshith shetty', 'india', 'rakshith@nesoulsoft.com', '9591539386', '2021-01-22 10:29:54', '2021-10-19 20:16:36', '2021-10-19 20:24:07', 1, 'pollsdemo01'),
(24, 'Mahesh', 'india', 'mahesh@gmail.com', '9591539386', '2021-01-22 10:40:11', '2021-01-22 11:52:00', '2021-01-22 14:09:29', 0, 'pollsdemo01'),
(25, 'rakshith shetty', 'india', 'rakshith@neosoulsoft.com', '9591539386', '2021-06-21 17:28:12', '2021-10-20 12:41:28', '2021-10-20 12:41:40', 0, 'pollsdemo01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_team_chat`
--
ALTER TABLE `tbl_team_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_team_chat`
--
ALTER TABLE `tbl_team_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
