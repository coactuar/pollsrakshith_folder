<?php
	require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Questions for Speaker</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body class="admin">
<div class="container-fluid main">
     <div class="row login-info links">   
        <div class="col-12">
            <div id="refProgress">
              <div id="refBar">
              
              </div>
            <div id="timeleft"></div>
            </div>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="questions"> 
                <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="20"></th>
                          <th width="200">Name</th>
                          <th>Question</th>
                          <th width="200">Asked At</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_questions where eventname='$event_name' and speaker='1' and answered='0' order by asked_at asc";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $i = 1;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['user_name']; ?></td>
                            <td><?php echo $data['user_question']; ?></td>
                            <td><?php 
                                $date=date_create($data['asked_at']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                            
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>
            </div>
        </div>
    </div>
</div>

<!-- <script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script> -->
<script>
var i = 0;
var t = <?php echo $refresh; ?>;
// alert(t)
var tleft = t;
function move() {
  if (i == 0) {
    i = 1;
    var elem = document.getElementById("refBar");
    var p = document.getElementById("timeleft");
    var width = 1;
    var id = setInterval(frame, 1000);
    function frame() {
    var d = new Date();
    var n = d.getTime();
      if (width >= 100) {
        //clearInterval(id);
        //i = 0;
        //t = <?php echo $refresh; ?>;
        //tleft = t;
        location.href = 'speaker.php';
      } else {
          if (width > 100)
          { width = 100; }
          else
          {
            width = width + (100 /t);
          }
          elem.style.width = width + "%";
          p.innerHTML = 'Auto refresh in ' + (tleft--) +' sec';
      }
    }
  }
}
move();

</script>

</body>
</html>