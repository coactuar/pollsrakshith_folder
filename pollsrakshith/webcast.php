<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $empid=$_SESSION["user_emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_emailid='$empid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_emailid"]);
            unset($_SESSION["user_loc"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="css/styles.css">

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 mb-1">
        <div class="col-10 offset-1 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
        </div>
    </div>
    <div class="row video-panel">
        <div class="col-12 col-lg-8">
            <div class="embed-responsive embed-responsive-16by9">
           
			   <iframe src="video.php" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </div>   
            <a href="#" class="btn btn-sm btn-warning mt-1" onClick="chatwithus('<?php echo $_SESSION['user_id']; ?>')">Talk to Us</a> 
            <a href="files/CoactCorporateProfile.pdf" target="_blank" download class="btn btn-sm btn-success mt-1">Download Company Profile</a> 
        </div>
        <div class="col-12 col-lg-4">
            <div class="question-box">
              <form id="question-form" method="post" role="form">
                      <div class="row">
                        <div class="col-10 offset-1">
                            <h6>Ask your Question:</h6>
                            <div id="message"></div>
                            <div class="form-group">
                               <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="6"></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="row mt-3">
                        <div class="col-10 offset-1">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_emailid" name="user_emailid" value="<?php echo $_SESSION['user_emailid']; ?>">
                            <input type="submit" class="btn btn-block btn-outline-warning" value="Submit Question" alt="Submit">
                            
                        </div>
                      </div>  
                </form>
            </div>   
            <div id="polls" style="display:none;">
            <div class="row mt-2">
                <div class="col-12">
                    <iframe id="poll-question" src="#" width="100%" height="350" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>    
            </div> 
        </div>
        
    </div>
    
</div>
<div id="team-chat"></div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

var pollTimer;
function chkPolls()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'chkpoll'},
         type: 'post',
         success: function(output) {
			   if(output != 0)
			   {    
                    if($('#poll-question').attr('src') != output)
                   {
                       $("#poll-question").attr("src", output);
                       $('#polls').css('display','');
                   }
			   }
               else{
                   $("#poll-question").attr("src", '#');
                   $('#polls').css('display','none');
               }
         }
    });
}
chkPolls();
pollTimer = setInterval(function(){ chkPolls(); }, 3000);

var getteamchat;

function chatwithus(from_user)
{
       var to_user_id = 'team'; //$(this).data('to');
       var from_user_id = from_user; //$(this).data('from');

       getTeamChatHistory(to_user_id, from_user_id);       
       
       make_teamchat_box(to_user_id, from_user_id);
       
       $('#chat_'+to_user_id).dialog({
           autoOpen: false,
           width: 400,
           height:360,
           title: 'Chat with Team COACT',
           position: { my: "right top", at: "right bottom-100px" }

           });

       $('#chat_'+to_user_id).dialog('open');
       var getteamchat = setInterval(function(){ getTeamChatHistory(to_user_id, from_user_id); }, 1000);
              
       $( '#chat_'+to_user_id).dialog({
          close: function() { clearInterval(getteamchat); 
          }
        });
}

function make_teamchat_box(to_user_id, from_user_id)
{
    var modal_content = '<div id="chat_' + to_user_id + '" class="user_dialog team_chat_box">';
    modal_content += '<div style="height:170px; border: 1px solid #dae0e5;background-color: #e9ecef; overflow-y:auto; margin-bottom:15px;" class="chat_history scroll" data-touser="'+to_user_id+'" id="chat_history_'+to_user_id+'">';
    modal_content += '</div>';
    modal_content += '<div class="form-group">';
    modal_content += '<input name="chat_message_'+to_user_id+'" id="chat_message_'+to_user_id+'" rows="1" class="form-control sendmsg" autocomplet="off">';
    modal_content += '</div><div class="form-group text-left">';
    modal_content += '<button type="button" name="send_teamchat" class="send_teamchat btn-sendmsg" data-to="'+to_user_id+'" data-from="'+from_user_id+'" class="btn btn-info">Send</button></div></div>';
    
    $('#team-chat').html(modal_content);
    
    
}
function getTeamChatHistory(to_user_id, from_user_id){
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getteamchathistory', to: to_user_id, from: from_user_id},
        type: 'post',
        success: function(response) {
            console.log(response);
            var tar = '#chat_history_'+to_user_id;
            var curr_hist = $(tar).html();
            if(response !== curr_hist)
            {
                $(tar).html(response);
                var myDiv = document.getElementById('chat_history_'+to_user_id);
                myDiv.scrollTop = myDiv.scrollHeight;
            }
            else{
            }
            
        }
    });
}

$(document).on('click','.send_teamchat',function(){
    console.log('send msg');
    var sendbtn = document. querySelector('.send_teamchat');
   sendbtn.disabled = true;
   var to_user_id = 'team'; // $(this).data('to');
   var from_user_id = $(this).data('from');
   var message = $('#chat_message_'+to_user_id).val();
   if(message.trim() !== '')
   {
       $.ajax({
          url: 'ajax.php',
          data: {action: 'sendTeamMessage', to: to_user_id, from: from_user_id, msg: message},
          type: 'post',
          success: function() {
              $('#chat_message_'+to_user_id).val('');
          }
      });
   }
   sendbtn.disabled = false;
    return false;
});
</script>
</body>
</html>